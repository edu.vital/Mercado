class Product < ActiveRecord::Base
  attr_accessible :description, :id_product, :price, :quantity_stock
end
